% requires JSONlab: https://www.mathworks.com/matlabcentral/fileexchange/33381-jsonlab--a-toolbox-to-encode-decode-json-files
% Based on https://github.com/mcordts/cityscapesScripts/blob/master/cityscapesscripts/preparation/json2instanceImg.py
% from the Cityscapes dataset: https://www.cityscapes-dataset.com/
path_to_gtfine = '/home/dylanturpin/data/cityscapes/gtFine_100_examples_only/';
% Full size output is 2048x1024.
% Output at each of the following scale factors (e.g. factor of 2 means
% output at 1024x512, 4 means 512x256, etc.).
scale_factors = [1 2 4 8];

% Enumerate paths
list_of_sets = {'train', 'val', 'test'};
paths = {}; % list of paths to poly jsons
for i=1:length(list_of_sets)
	path_to_set = [path_to_gtfine list_of_sets{i} '/'];
	cities = dir(path_to_set);
   
	for j=1:length(cities)
		if cities(j).name(1) ~= '.' % ignore hidden folders
			path_to_city = [path_to_set cities(j).name '/'];
			files = dir(path_to_city);
			
			for k=1:length(files)
				if endsWith(files(k).name,'.json')
					path_to_file = [path_to_city files(k).name];
					paths{end+1} = path_to_file;
				end
			end
		end
	end
end

map_name_to_label = get_map_name_to_label();

% Now walk through paths, read polygon jsons, rasterize them as
% matrices of polygon boundary points with instanceIds, save them as
% *_boundaryInstanceIds.png
% instanceId scheme:
%	1) if label.hasInstances=false, instanceId=label.trainId
%	2) if label.hasInstance=true, instanceId = label.trainId*1000 + counter
%	3) if label.name ends in 'group' (e.g. 'cargroup')
%		then this is a label that usually has instances, but in this case
%		instances could not be separated, so instanceId = label.trainId
for path_counter=1:length(paths)
	path_counter
	data = loadjson(paths{path_counter});	
	background = map_name_to_label('unlabeled').trainId;
	
	label_names = map_name_to_label.keys();
	map_name_to_n_instances = containers.Map(label_names, zeros(1, length(label_names)));

	for scale_counter=1:length(scale_factors)
		scale = scale_factors(scale_counter);
		H = data.imgHeight / scale;
		W = data.imgWidth / scale;
		
		img = zeros(H, W, 'uint16');
		img(:) = background;


		for object_counter=1:length(data.objects)
			% calculate an instanceId
			label_name = data.objects{object_counter}.label;

			% if label is not known, but ends in 'group' (e.g. 'cargroup')
			% try to remove the 'group' and see if that works
			isGroup = false;
			if ~map_name_to_label.isKey(label_name) && endsWith(label_name, 'group')
				label_name = label_name(1:end-length('group'));
				isGroup = true;
			end

			if ~map_name_to_label.isKey(label_name)
				['Error unknown label: ' label_name]
				continue
			end

			label = map_name_to_label(label_name);
			instanceId = label.trainId;

			% if label has instances, isn't grouped and isn't background
			% instanceId = 1000 * labelId + counter
			% Important to check not background. Because e.g. trailer
			% counts as background since ignoreInEval=true but has
			% instances
			if label.hasInstances && ~isGroup && instanceId ~= background
				instanceId = instanceId * 1000 + map_name_to_n_instances(label_name);
				map_name_to_n_instances(label_name) = map_name_to_n_instances(label_name) + 1;
			end

			poly_x = data.objects{object_counter}.polygon(:,1);
			poly_y = data.objects{object_counter}.polygon(:,2);
			poly_x = floor(poly_x / scale);
			poly_y = floor(poly_y / scale);
			poly_x(poly_x == 0) = 1;
			poly_y(poly_y == 0) = 1;

			% Convert polygon into logical mask matrix.
			% (used to occlude already drawn boundary lines)
			mask = poly2mask(poly_x, poly_y, H, W);
			% Use mask to occlude already drawn boundaries.
			img(mask) = background;

			% loop to draw the lines of the polygon
			for k=1:numel(poly_x)-1
				[line_x, line_y] = bresenham(poly_x(k), poly_y(k), poly_x(k+1), poly_y(k+1));
				% force invalid indices to 1 or max
				line_x(line_x <= 0) = 1;
				line_y(line_y <= 0) = 1;
				line_x(line_x > W) = W;
				line_y(line_y > H) = H;
				idx = sub2ind(size(img), line_y, line_x);
				img(idx) = instanceId;
			end
			% one more line to close the polygon
			[line_x, line_y] = bresenham(poly_x(end), poly_y(end), poly_x(1), poly_y(1));
			line_x(line_x <= 0) = 1;
			line_y(line_y <= 0) = 1;
			line_x(line_x > W) = W;
			line_y(line_y > H) = H;

			idx = sub2ind(size(img), line_y, line_x);
			img(idx) = instanceId;
		end % end poly loop
	
		% save downsammpled versions
		new_path = replace(paths{path_counter}, 'polygons.json', ['boundaryInstanceIds_' int2str(W) 'x' int2str(H) '.png']);
		imwrite(img, new_path);
	end % end scales loop
end % end paths loop 