% Returns a map from label names to label structs.
% Data is copy pasted from https://github.com/mcordts/cityscapesScripts/blob/master/cityscapesscripts/helpers/labels.py
% from the Cityscapes dataset: https://www.cityscapes-dataset.com/
function map_name_to_label = get_map_name_to_label()
label_cells = {
    %  name                     id    trainId   category            catId     hasInstances   ignoreInEval   color
    {  'unlabeled'            ,  0 ,      255 , 'void'            , 0       , false        , true         , [  0,  0,  0] },
    {  'ego vehicle'          ,  1 ,      255 , 'void'            , 0       , false        , true         , [  0,  0,  0] },
    {  'rectification border' ,  2 ,      255 , 'void'            , 0       , false        , true         , [  0,  0,  0] },
    {  'out of roi'           ,  3 ,      255 , 'void'            , 0       , false        , true         , [  0,  0,  0] },
    {  'static'               ,  4 ,      255 , 'void'            , 0       , false        , true         , [  0,  0,  0] },
    {  'dynamic'              ,  5 ,      255 , 'void'            , 0       , false        , true         , [111, 74,  0] },
    {  'ground'               ,  6 ,      255 , 'void'            , 0       , false        , true         , [ 81,  0, 81] },
    {  'road'                 ,  7 ,        0 , 'flat'            , 1       , false        , false        , [128, 64,128] },
    {  'sidewalk'             ,  8 ,        1 , 'flat'            , 1       , false        , false        , [244, 35,232] },
    {  'parking'              ,  9 ,      255 , 'flat'            , 1       , false        , true         , [250,170,160] },
    {  'rail track'           , 10 ,      255 , 'flat'            , 1       , false        , true         , [230,150,140] },
    {  'building'             , 11 ,        2 , 'construction'    , 2       , false        , false        , [ 70, 70, 70] },
    {  'wall'                 , 12 ,        3 , 'construction'    , 2       , false        , false        , [102,102,156] },
    {  'fence'                , 13 ,        4 , 'construction'    , 2       , false        , false        , [190,153,153] },
    {  'guard rail'           , 14 ,      255 , 'construction'    , 2       , false        , true         , [180,165,180] },
    {  'bridge'               , 15 ,      255 , 'construction'    , 2       , false        , true         , [150,100,100] },
    {  'tunnel'               , 16 ,      255 , 'construction'    , 2       , false        , true         , [150,120, 90] },
    {  'pole'                 , 17 ,        5 , 'object'          , 3       , false        , false        , [153,153,153] },
    {  'polegroup'            , 18 ,      255 , 'object'          , 3       , false        , true         , [153,153,153] },
    {  'traffic light'        , 19 ,        6 , 'object'          , 3       , false        , false        , [250,170, 30] },
    {  'traffic sign'         , 20 ,        7 , 'object'          , 3       , false        , false        , [220,220,  0] },
    {  'vegetation'           , 21 ,        8 , 'nature'          , 4       , false        , false        , [107,142, 35] },
    {  'terrain'              , 22 ,        9 , 'nature'          , 4       , false        , false        , [152,251,152] },
    {  'sky'                  , 23 ,       10 , 'sky'             , 5       , false        , false        , [ 70,130,180] },
    {  'person'               , 24 ,       11 , 'human'           , 6       , true         , false        , [220, 20, 60] },
    {  'rider'                , 25 ,       12 , 'human'           , 6       , true         , false        , [255,  0,  0] },
    {  'car'                  , 26 ,       13 , 'vehicle'         , 7       , true         , false        , [  0,  0,142] },
    {  'truck'                , 27 ,       14 , 'vehicle'         , 7       , true         , false        , [  0,  0, 70] },
    {  'bus'                  , 28 ,       15 , 'vehicle'         , 7       , true         , false        , [  0, 60,100] },
    % manually change caravan and trailer trainIds from 255 to 19 and 20
	{  'caravan'              , 29 ,       19 , 'vehicle'         , 7       , true         , true         , [  0,  0, 90] },
    {  'trailer'              , 30 ,       20 , 'vehicle'         , 7       , true         , true         , [  0,  0,110] },
    {  'train'                , 31 ,       16 , 'vehicle'         , 7       , true         , false        , [  0, 80,100] },
    {  'motorcycle'           , 32 ,       17 , 'vehicle'         , 7       , true         , false        , [  0,  0,230] },
    {  'bicycle'              , 33 ,       18 , 'vehicle'         , 7       , true         , false        , [119, 11, 32] },
    % manually changed license plates to 255
	{  'license plate'        , -1 ,       255 , 'vehicle'         , 7       , false        , true         , [  0,  0,142] }
};

names = {};
labels = struct('name', {}, 'trainId', {}, 'hasInstances', {});
for i=1:length(label_cells)
	names{i} = label_cells{i}{1};
	labels{i}.name = label_cells{i}{1};
	labels{i}.trainId = label_cells{i}{3};
	labels{i}.hasInstances = label_cells{i}{6};
end

map_name_to_label = containers.Map(names, labels);

end

